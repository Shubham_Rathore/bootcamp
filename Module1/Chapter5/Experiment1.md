>Aim
<p>To find the root of the transcendental equation using Newton Raphson and a successive approximation method.</p>

>Procedure

> > <span>Example</span>

![Example](images/lastequation.png "Example")

> > 1 Provide f(x) as x^2-4*x+4.<br>
> > 2 Provide derivative df(x) as 2*x-4.<br>
> > 3 Provide g(x) as (4x-4)^0.5<br>
> > 4 Hit solve<br>

<br>

![LastExampe](images/example.jpg)

> Theory

> > <b>Newton-Raphson Method :</b><br><br>
> <p>This iterative method of determining the roots of the function is termed after Issac Newton and Joseph Raphson. According to this method, the X-intercept of the tangent drawn at the initial guess point is the better approximation for the root of the function.</p><br><br>
> <p>E.g. Fig. 1 shows the graph of function <img src="images/fun.png">. If the initial guess of the root is <img src="images/x0.png">, then the tangent is drawn at the point <img src="images/closedfun.png">. The X-intercept of this tangent <img src="images/x1.png"> is considered as the better approximation of the root.</p>

![Figure1](images/fig_1.jpg)
<br>
Fig. 1 : Newton-Raphson Method
<br>
Thus using simple geometrical concepts, the new guess <img src="images/x1.png">can be determined.
<p style="font-size:18px;padding-top: 10px;word-spacing: 5px;">The equation of tangent at point <img src="images/closedfun.png"></p>